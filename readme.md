# Содержание
- [AUTH Router](#authRouter)
    * [Регистрация](#authSignup)
    * [Авторизация](#authAuth)
    * [Выход](#authLogout)
- [CHAT Router](#chatRouter)
    * [Поиск собеседника, выбор собеседника](#chatSearch)
    * [Удаление сообщения или собеседника](#chatDelete)
    * [Информация о собеседнике](#chatInfo)
    * [Получение аватара](#chatGetava)
- [SETTINGS Router](#settingsRouter)
    * [Получить инфо для настроек](#settingsGet)
    * [Изменить username](#settingsCngun)
    * [Изменить пароль](#settingsCngpwd)
    * [Изменить описание профиля](#settingsSetbio)
    * [Установить аватар](#settingsSetava)

# Работа приложения
После запроса к логике роутера авторизации (авторизация или регистрация), клиент получает токен сессии, который необходим в каждом запросе для идентификации пользователя и для проверки авторизации. После успешной авторизации производим подключение к сокету (метка 1 в примере использования сокета клиентом).
Чтобы найти собеседника используем логику Чат роутера, **/chat** c query параметром username, значение параметра - имя собеседника. Запрос **/chat** c query параметром id - uuid собеседника, используется для начала переписки. После этого запроса необходимо инициировать событие **read** сокета (метка 5). 
Для отправки сообщения пользуемся логикой сокета (метка 2). При отправке сообщения API сохраняет его в базу и возвращает формализованное сообщение по событию **messageLoopback** (метка 3). Сообщение отправляется собеседнику, при прочтении генерируется событие **read** (метка 5), приходит **readMessageLoopback** (метка 4)

<a name="authRouter"></a>
# AUTH Router - ''
<a name="authSignup"></a>
### POST /signup
**request:**
``` javascript
    body: {
        username: string required,
        password: string required
    }
```
**response:** 
``` javascript
    body: {
        token: string uuid,
        message: 'sign up is ok',
        status: 200,
        userInfo: {},
        peers: []
    }
```

<a name="authAuth"></a>
### POST /auth
**request:**
``` javascript
    body: {
        username: string required,
        password: string required
    }
```
**response:**
``` javascript
    body: {
            token: string uuid,
            message: 'sign up is ok',
            status: 200,
            peers: [
                {
                    id: string uuid,
                    username: string,
                    avatar: null || {
                        id: string uuid,
                        path: string,
                        createdDate: Date,
                        updatedDate: Date
                    },
                    isUnreadMessages: boolean
                },
            ],
            userInfo: {
                id: string uuid,
                username: string,
                password: null,
                createdDate: Date,
                updatedDate: Date,
                avatar: null || {
                    id: string uuid,
                    path: string,
                    createdDate: Date,
                    updatedDate: Date
                }
            }
        }
```

<a name="authLogout"></a>
### DELETE /logout
**request:**
``` javascript
    headers: {
        token: string uuid required
    }
```

**response:**
``` javascript
    body: {
        message: `token ${token.id} deactivated successful`,
        status: 200
    }
```

<a name="chatRouter"></a>
# chat router - /chat
<a name="chatSearch"></a>
### GET ''

Данные, возвращаемые api, различаются от параметров запроса:
- Если в query параметрах запроса есть username, API вернет всех пользователей, username которых начинается со строки в запросе. 
- Если в query параметрах запроса нет username, но есть id - API вернет контекст выбранного пользователя, в том числе историю сообщений между отправителем запроса и выбранным пользователем. Для такого типа запроса существует еще один необязательный query параметр **count**, который влияет на поведение следующим образом: по умолчанию count равен нулю, API возвращает 20 последних сообщений. При count = 1, API вернет сообщения с 21 по 40, и так далее.
- Если query параметры отсутствуют, API вернет информацию о всех собеседниках пользователя

**request**
```javascript
    headers: {
        token: string uuid required
    }
    query: {
        id: string uuid, //peer id собеседника, юзать для начала переписки 
        username: string, //autosuggest users by username part
        count: number //в режиме диалога API выплевывает по 20 сообщений, указание этого параметра сообщает, какую по номеру партию сообщений необходимо вернуть. По умолчанию 0.
    }
```
**response if query username != null**
```javascript
    body: {
        message: 'ok',
        status: 200,
        peers: [
            {
                id: string uuid,
                username: string,
                avatar: null || {
                    id: string uuid,
                    path: string,
                    createdDate: Date,
                    updatedDate: Date
                }
            },
        ]
    }
```
**response if query username == null && id != null**
```javascript
    body: {
        message: 'ok',
        status: 200,
        peer: {
            id: string uuid,
            username: string,
            bio: null || string,
            createdDate: Date,
            avatar: null || {
                id: string uuid,
                path: string,
                createdDate: Date,
                updatedDate: Date
            }
        },
        messages: null || [
            {
                id: string uuid,
                from: string uuid,
                to: string uuid,
                text: string,
                status: string,
                createdDate: Date,
                updatedDate: Date
            },
        ]
        
    }
```
**response if query username == null && id == null**
```javascript
    body: {
        message: 'ok',
        status: 200,
        peers: [
            {
                id: string uuid,
                username: string,
                avatar: null || {
                    id: string uuid,
                    path: string,
                    createdDate: Date,
                    updatedDate: Date
                },
                isUnreadMessages: boolean
            },
        ]        
    }
```
**Примеры запросов:**
1. username:
```javascript
    const url = "http://192.168.1.1/chat?id=f2486047-b0b1-4ff9-aa31-16b7a6c30731"
    fetch(url, {
            method: "GET",
            headers: {
                token: "4678d91a-b28d-4805-91c8-4398a93f6176"
            }
        }
    })
```
2. id:
```javascript
    const url = "http://192.168.1.1/chat?username=john"
    fetch(url, {
            method: "GET",
            headers: {
                token: "4678d91a-b28d-4805-91c8-4398a93f6176"
            }
        }
    })
```
3. Без параметров:
```javascript
    const url = "http://192.168.1.1/chat"
    fetch(url, {
            method: "GET",
            headers: {
                token: "4678d91a-b28d-4805-91c8-4398a93f6176"
            }
        }
    })
```

<a name="chatDelete"></a>
### DELETE ''
**request**
```javascript
    headers: {
        token: string uuid required
    },
    query: {
        peerId: string uuid,
        messageId: string uuid,
    }
```
**response if query peerId != null**
```javascript
    body: {
        message: 'ok',
        status: 200,
    }
```
**response if query peerId == null && messageId != null**
```javascript
    body: {
        message: 'ok',
        status: 200,
    }
```

<a name="chatInfo"></a>
### GET /info
**request**
```javascript
    headers: {
        token: string uuid required
    },
    query: {
        id: string uuid required
    }
``` 
**response**
```javascript
    body: {
        message: 'ok',
        status: 200,
        peer: {
            id: string uuid,
            username: string,
            bio: null || string,
            createdDate: Date,
            avatar: null || {
                id: string uuid,
                path: string,
                createdDate: Date,
                updatedDate: Date
            }
        }
    }
```

<a name="chatGetava"></a>
### GET /getava
**request**
```javascript
    headers: {
        token: string uuid required
    },
    query: {
        avatarId: string uuid required
    }
```
**response**
```javascript
    body: {
        file
    }
```

<a name="settingsRouter"></a>
# settings router - /settings
<a name="settingsGet"></a>
### GET ''
**request**
```javascript
    headers: {
        token: string uuid required
    },
```
**response**
```javascript
    body: {
        message: 'ok',
        status: 200,
        avatar: null || {
                id: string uuid,
                path: string,
                createdDate: Date,
                updatedDate: Date
        },
        username: string,
        password: null,
        bio: null || string,
        createdDate: Date,
        updatedDate: Date
    }
```
<a name="settingsCngun"></a>
### PATCH /cngun
**request**
```javascript
    headers: {
        token: string uuid required
    },
    body: {
        newUsername: string required
    }
```
**response**
```javascript
    body: {
        message: 'ok',
        status: 200,
        user: {
            id: string uuid,
            username: string,
            password: null,
            bio: string,
            createdDate: Date,
            updatedDate: Date
        }
    }
```
<a name="settingsCngpwd"></a>
### PATCH /cngpwd
**request**
```javascript
    headers: {
        token: string uuid required
    },
    body: {
        oldPassword: string required,
        newPassword: string required
    }
```
**response**
```javascript
    body: {
        message: 'ok',
        status: 200
    }
```
<a name="settingsSetbio"></a>
### PATCH /setbio
**request**
```javascript
    headers: {
        token: string uuid required
    },
    body: {
        bio: string
    }
```
**response**
```javascript
    body: {
        message: 'ok',
        status: 200,
        user: {
            id: string uuid,
            username: string,
            password: null,
            bio: string,
            createdDate: Date,
            updatedDate: Date
        }
    }
```
<a name="settingsSetava"></a>
### POST /setava
**request**
```javascript
    headers: {
        token: string uuid required,
        Content-Type: multipart/form-data
    },
    body: {
        avatar: file
    }
```
**response**
```javascript
    body: {
        message: 'ok',
        status: 200,
        avatarId: string uuid,
        createdDate: Date,
        updatedDate: Date
    }
```