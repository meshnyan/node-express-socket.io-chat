const io = require('socket.io-client')
/// 1
let socket = io.connect('http://localhost:3001', {
    query: {
        token: '703a801a-8469-47cb-b1d5-e4062c02d9a9'
    }
});

console.log('connected')

const state = {
    chatWith: 'f2486047-b0b1-4ff9-aa31-16b7a6c30731'
}
// для отправки сообщений, таймаут для тестов

// setTimeout(() => {
//     console.log('message happend)
//     socket.emit('message', {
//         to: 'f2486047-b0b1-4ff9-aa31-16b7a6c30731',
//         text: "text2 test1"
//      })
// }, 1000)

///2
socket.on('message', function (data) {
    console.log(data);
    if (data.from === state.chatWith) {
        console.log('message got in runtime')
        socket.emit('read', {
            from: data.from
        })
    }
});

///3
socket.on('messageLoopback', data => {
    console.log('message loopback: ', data)
        {
            //код для изменений состояния приложения
        }
})

///4
socket.on('readMessagesLoopback', data => {
    console.log('read messages loopback: ', data)
    if (data.from === state.chatWith) {
        console.log('changes')
        {
            //код для изменений состояния приложения
        }
    }
})

// код для отправки сообщения о том, что сообщение прочитано.
// как только пользователь зашел в чат генерируется это событие

///5
setTimeout(() => {
    console.log('read happend')
    socket.emit('read', {
        from: state.chatWith
    })
}, 1000)
