import {Token} from '../entities/token'
import { Repository } from 'typeorm';
import { Peers } from '../entities/peers';
import { User } from '../entities/user';
import { Messages } from '../entities/messages';
import { getUserRepository } from '../db/user.repository'
import { getPeersRepository } from '../db/peers.repository'
import { getTokenRepository } from '../db/token.repository'
import { getMessagesRepository } from '../db/messages.repository'
import { Avatars } from '../entities/avatars';
import { getAvatarsRepository } from '../db/avatars.repository';

export namespace Singleton {
	
    interface Instance {
        userRepo: Repository <User>,
        peersRepo: Repository <Peers>,
        tokenRepo: Repository <Token>,
        messagesRepo: Repository <Messages>,
        avatarsRepo: Repository <Avatars>
    }

    let instance :Instance;

    export function getInstance(connection = null) :Instance {
        if(connection)
            instance = {
                userRepo: getUserRepository(connection),
                peersRepo: getPeersRepository(connection),
                tokenRepo: getTokenRepository(connection),
                messagesRepo: getMessagesRepository(connection),
                avatarsRepo: getAvatarsRepository(connection)
            };
        return instance
    }
  }