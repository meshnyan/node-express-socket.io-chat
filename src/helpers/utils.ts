import { Repository, Like, In, Not, getManager } from "typeorm"
import { User } from "../entities/user"
import {TokenStatus, Token} from '../entities/token'
import validate from 'uuid-validate'
import { Peers } from "../entities/peers"
import { Messages, MessageStatus } from "../entities/messages"
import { Avatars } from "../entities/avatars"
import path from 'path'

export const dbSave = async (repo: Repository <any>, data: object) => {
    const a = repo.create(data);
    return repo.save(a);
}

export const findUser = async (repo : Repository <User>, username : string): Promise<User> => {
    return repo.findOne({
        where: {
            username
        },
        relations: ["avatar"]
    })
}

export const findToken = async (repo : Repository <Token>, token : string): Promise<Token> => {
    return repo.findOne({
        where: {
          id: token,
          status: TokenStatus.ACTIVE
        }
    })
}

export const findAllActiveTokens = async (repo : Repository <Token>): Promise<Token[]> => {
    return repo.find({
        where: {
          status: TokenStatus.ACTIVE
        }
    })
}

export const isTokenActive = async (repo: Repository <Token>, sessionToken: string): Promise<Boolean> => {

    if (validate(sessionToken, 4)) {
        const dbToken = await findToken(repo, sessionToken)
        if (dbToken) {
            
            dbSave(repo, {
                ...dbToken,
                lastActivity: new Date()
            })
            return true
        } else {
            return false
        }
    } else {
        return false
    }
}

export const findUserByToken = async (repo: Repository<Token>, token: string) => {
    const tokenInfo = await repo.findOne({
        relations: ["user"],
        where: {
            id: token
        }
    });
    return tokenInfo.user
}


export const findUserPeers = async (repoPeers: Repository<Peers>, repoUser: Repository<User>, user: User) => {
    const peersInfo = await repoPeers.find({
        where: {
            user,
            deleted: false
        },
        select: ["peerId", "isUnreadMessages", "lastMessageText", "lastMessageFrom"]
    })

    const peersId = peersInfo.map((item) => {
        return item.peerId
    })

    if(!peersId.length) {
        return []
    }

    let peers = await repoUser.find({
        where: {
            id: In (peersId)
        },
        select: ["id", "username"],
        relations: ["avatar"]
    })

    peers = peers.map((item, index) => {
        let newItem = {...item, 
        isUnreadMessages: peersInfo[index].isUnreadMessages,
        lastMessageText: peersInfo[index].lastMessageText,
        lastMessageFrom: peersInfo[index].lastMessageFrom
    }
        return newItem
    })

    return peers
}

export const findUsersByUsername = async (repo: Repository<User>, username: string) => {
    const users = await repo.find({
        where: {
            username: Like(`%${username}%`)
        },
        select: ["id", "username"],
        relations: ["avatar"]
    })
    return users
}

export const findUserById = async (repo: Repository<User>, id: string) => {
    return repo.findOne({
        relations: ["avatar"],
        where: {
            id
        },
        select: ["id", "username", "createdDate", "bio"]
    })
}

export const deleteAnotherTokens = async (repo: Repository <Token>, user: User, token: string) => {
    const tokens = await repo.find({
        where: {
            user,
            status: TokenStatus.ACTIVE,
            id: Not(token)
        }
    })
    tokens.forEach(item => {
        dbSave(repo, {
            id: item.id,
            status: TokenStatus.DELETED
        })
    })
}

export const isPeer = async (repo: Repository<Peers>, user: User, peerId: string) => {
    const peer = await repo.findOne({
        where: {
            user,
            peerId
        }
    })

    if(peer) {
        return true
    } else {
        return false
    }
}

export const findUsersPeerById = async (repo: Repository <Peers>, user: User, peerId: string) =>{
    return repo.findOne({
        where: {
            user,
            peerId
        }
    })
}

export const deletePeer = async (repo: Repository<Peers>, peerId: string, user: User) => {
    const peer = await repo.findOne({
        where: {
            user,
            peerId
        }
    })
    peer.deleted = true;
    repo.save(peer)
    return
}

export const saveMessage = async (repo: Repository<Messages>, from: string, to: string, text: string, status: MessageStatus = MessageStatus.DELIVERED): Promise <Messages> => {
    return dbSave(repo, {
        from,
        to,
        text,
        status
    })
}

export const fetchChatMessages = async (repo: Repository <Messages>, user: User, peer: User, count: number) => {
    const messages = await repo.find({
        where: [
            {from: user.id, to: peer.id, status: MessageStatus.DELIVERED},
            {from: peer.id, to: user.id, status: MessageStatus.DELIVERED},
            {from: user.id, to: peer.id, status: MessageStatus.READ},
            {from: peer.id, to: user.id, status: MessageStatus.READ},
        ],
        order: {
            createdDate: "DESC"
        },
        skip: count*20,
        take: 20,
    })

    messages.sort((a, b) => {
        if (a.createdDate > b.createdDate) {
            return 1;
        }
        if (a.createdDate < b.createdDate) {
            return -1;
        }
        return 0;
    })

    return messages
}

export const deleteMessage = async (repo: Repository <Messages>, id) => {
    let a = await repo.findOne({
        where: {
            id
        }
    })
    a = {...a,
        status: MessageStatus.DELETED
    }
    repo.save(a)
}

export const findUnreadMessages = async (repo: Repository <Messages>, user: User, from: string) => {
    return repo.find({
        where: {
            to: user.id,
            from,
            status: MessageStatus.DELIVERED,
        }
    })
}

export const switchDeliveredStatusToRead = async (messagesRepo: Repository <Messages>, peerRepo: Repository <Peers>, user: User, peer: Peers) => {
    const deliveredMessages = await messagesRepo.find({
        where: {
            to: user.id,
            from: peer.peerId,
            status: MessageStatus.DELIVERED,
        },
    })
    
    const readMessages = deliveredMessages.map(item => {
        item.status = MessageStatus.READ;
        return item
    })
    
    const updateResult = await messagesRepo.update(
        {
            to: user.id,
            from: peer.peerId,
            status: MessageStatus.DELIVERED,
        },
        {
            status: MessageStatus.READ,
        },
    );

    if (updateResult.affected > 0) {
        await peerRepo.update(
            {
                id: peer.id
            },
            {
                isUnreadMessages: false
            }
        )
        return readMessages
    }
} 

export const findUsersAvatarId = async (repo: Repository <Avatars>, user) => {
    return repo.findOne({
        where: {
            user
        }
    })
}

export const findUsersAvatarPath = async (repo: Repository <Avatars>, id) => {
    const avatarInfo = await repo.findOne({
        where: {
            id
        },
        select: ['path']
    })
    return path.resolve('', avatarInfo.path)
    
}