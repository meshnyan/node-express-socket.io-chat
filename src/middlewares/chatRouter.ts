import express from "express";
import validate from "uuid-validate"

import * as Utils from '../helpers/utils'
import {Singleton} from '../helpers/singleton'
import checkTokenIsActive from '../middlewares/checkTokenIsActive'
import { asyncHandler } from "../helpers/async-handler";

const chatRouter = express.Router();

chatRouter.use(asyncHandler(checkTokenIsActive));

chatRouter.get('', async (req, res, next) => {
    const request = {
        peerId: req.query.id as string,
        username: req.query.username as string,
        token: req.headers.token,
        messageCounter: typeof +req.query.count === 'number' ? +req.query.count : 0
    }

    if (request.username) {
        const peers = await Utils.findUsersByUsername(Singleton.getInstance().userRepo, request.username);
        res.send({
            message: "ok",
            status: 200,
            peers
        })
    } else if(request.peerId) {
        if(validate(request.peerId, 4)) {
            const peer = await Utils.findUserById(Singleton.getInstance().userRepo, request.peerId);
            const user = await Utils.findUserByToken(Singleton.getInstance().tokenRepo, request.token as string)
            const messages = await Utils.fetchChatMessages(Singleton.getInstance().messagesRepo, user, peer, request.messageCounter)
            
            if(!await Utils.isPeer(Singleton.getInstance().peersRepo, user, request.peerId)) {
                Utils.dbSave(Singleton.getInstance().peersRepo, {
                    user,
                    peerId: request.peerId
                })
            }
            res.send({
                message: "ok",
                status: 200,
                peer,
                messages
            })
        }  else {
            res.status(400).send({
                message: "invalid peer id",
                status: 400,
            })
        }
    } else {
        const user = await Utils.findUserByToken(Singleton.getInstance().tokenRepo, request.token as string)
        const peers = await Utils.findUserPeers(Singleton.getInstance().peersRepo, Singleton.getInstance().userRepo, user)
        res.send({
            message: "ok",
            status: 200,
            peers
        })
    }
})

chatRouter.delete('', async (req, res) => {
    const request = {
        token: req.headers.token,
        peerId: req.query.peerId as string,
        messageId: req.query.messageId as string
    }
    if (request.peerId && validate(request.peerId, 4)) {
        const user = await Utils.findUserByToken(Singleton.getInstance().tokenRepo, request.token as string);
        Utils.deletePeer(Singleton.getInstance().peersRepo, request.peerId, user);
        res.send({
            message: "ok",
            status: 200
        })
    } else if (request.messageId && validate(request.messageId, 4)) {
        Utils.deleteMessage(Singleton.getInstance().messagesRepo, request.messageId)
        res.send({
            message: 'ok',
            status: 200
        })
    } else {
        res.status(400).send({
            message: "request params not found or incorrect",
            status: 400
        })
    }  
})

chatRouter.get('/info', async (req, res, next) => {
    const request = {
        peerId: req.query.id as string
    }

    if (request.peerId && validate(request.peerId, 4)) {
        const peer = await Utils.findUserById(Singleton.getInstance().userRepo, request.peerId)
        res.send({
            message: "ok",
            status: 200,
            peer
        })
    } else {
        res.status(400).send({
            message: "request params not found or incorrect",
            status: 400
        })
    }  
})

chatRouter.get('/getava', async (req, res, next) => {
    const request = {
        id: req.query.avatarId as string
    }

    if(!((request.id) && validate(request.id, 4))) {
        res.status(400).send({
            message: "avatar id is incorrect or missing",
            status: 400
        })
        return
    }

    res.sendFile((await Utils.findUsersAvatarPath(Singleton.getInstance().avatarsRepo, request.id)), (e) => {
        if (e) {
                res.status(404).send({
                message: 'file not found',
                status: 404
            })
        }
    })
})

export default chatRouter;