import express from "express";
import validate from "validate.js";
import bcrypt from 'bcrypt';

import * as Utils from '../helpers/utils'
import {Singleton} from '../helpers/singleton'
import { TokenStatus } from '../entities/token';
import checkTokenIsActive from '../middlewares/checkTokenIsActive'

const authRouter = express.Router();

authRouter.post('/signup', async (req, res) => {
    const request = {
        username: req.body.username,
        password: req.body.password,
    }

    const response = {
        token: null,
        message: 'sign up is ok',
        status: 200,
        userInfo: {},
        peers: [],
    }

    if(!validate(request, {
        username: {type: "string", presence: true},
        password: {type: "string", presence: true}
    })) {
        const user = await Utils.findUser(Singleton.getInstance().userRepo, request.username)
        
        if (user) {
            response.message = 'Conflict. User is already exist.';
            response.status = 409;
            res.status(409).send(response);
            return
        } else {
            const pwdToSave = bcrypt.hashSync(request.password, 10)
            console.log(request)
            const addedUser = await Utils.dbSave(Singleton.getInstance().userRepo, {
                username: request.username,
                password: pwdToSave,
            })

            const userToken = await Utils.dbSave(Singleton.getInstance().tokenRepo, {
                user: addedUser,
                lastActivity: new Date()
            })

            response.token = userToken.id
            response.peers = await Utils.findUserPeers(Singleton.getInstance().peersRepo, Singleton.getInstance().userRepo, addedUser)
            response.userInfo = {...addedUser,
                password: null};
        }
    } else {
        response.message = "Bad request. Check username or pwd fields";
        response.status = 400
        res.status(400).send(response)
    }

    res.send(response)
    
})

authRouter.post('/auth', async (req, res) => {
    const request = {
        username: req.body.username,
        password: req.body.password,
    }

    const response = {
        token: null,
        message: 'Auth is ok',
        status: 200,
        peers: [],
        userInfo: {}
    }

    if(!validate(request, {
        username: {type: "string", presence: true},
        password: {type: "string", presence: true}
    })) {  
        const user = await Utils.findUser(Singleton.getInstance().userRepo, request.username);
        if (user) {
            response.token = await bcrypt.compare(request.password, user.password).then( async (result) => {
                if (result) {
                    const newToken = await Utils.dbSave(Singleton.getInstance().tokenRepo, {
                        user,
                        lastActivity: new Date()
                    });
                    return newToken.id;
                }
            })

            if(!response.token) {
                response.message = 'Incorrect password',
                response.status = 404
                res.status(404).send(response)
                return
            }
        } else {
            response.message = 'Such user does not exist',
            response.status = 404
            res.status(404).send(response)
            return
        }
        response.userInfo = {...user,
            password: null,
        };
        response.peers = await Utils.findUserPeers(Singleton.getInstance().peersRepo, Singleton.getInstance().userRepo, user)
    } else {
        response.message = "Bad request. Check username or pwd fields";
        response.status = 400
        res.status(400).send(response)
    }
    res.send(response)
})

authRouter.delete('/logout', checkTokenIsActive, async (req, res) => {
    const token = await Utils.findToken(Singleton.getInstance().tokenRepo, req.headers.token as string);
    token.status = TokenStatus.DELETED;
    Singleton.getInstance().tokenRepo.save(token)
    res.send({
        message: `token ${token.id} deactivated successful`,
        status: 200
    })
})

export default authRouter;