import express from "express";
import validate from "uuid-validate";
import bcrypt from 'bcrypt';

import * as Utils from '../helpers/utils'
import {Singleton} from '../helpers/singleton'
import checkTokenIsActive from '../middlewares/checkTokenIsActive'
import { asyncHandler } from "../helpers/async-handler";
import { upload } from "../helpers/diskStorage"
import multer from "multer";
import fs from "fs";

const settingsRouter = express.Router();

settingsRouter.use(asyncHandler(checkTokenIsActive));

settingsRouter.get('', async (req, res, next) => {
    const request = {
        token: req.headers.token,
    }
    let user = await Utils.findUserByToken(Singleton.getInstance().tokenRepo, request.token as string);
    let avatar = await Utils.findUsersAvatarId(Singleton.getInstance().avatarsRepo, user);
    res.send({
        message: 'ok',
        status: 200,
        avatar,
        ...user,
        password: null,
        
    })
})

settingsRouter.patch('/cngun', async (req, res, next) => {
    const request = {
        token: req.headers.token,
        newUsername: req.body.newUsername
    } 
    let test = await Utils.findUsersByUsername(Singleton.getInstance().userRepo, request.newUsername)
    if (test.length) {
        res.send({
            message: 'such username is already exist',
            status: 200
        })
    } else {
        let user = await Utils.findUserByToken(Singleton.getInstance().tokenRepo, request.token as string);
        user = {...user, 
        username: request.newUsername}
        Utils.dbSave(Singleton.getInstance().userRepo, user)
        res.send({
            message: 'ok',
            status: 200,
            user: {...user,
            password: null}
        })
    }
})

settingsRouter.patch('/cngpwd', async (req, res, next) => {
    const request = {
        token: req.headers.token,
        oldPassword: req.body.oldPassword,
        newPassword: req.body.newPassword,
    }
    let user = await Utils.findUserByToken(Singleton.getInstance().tokenRepo, request.token as string);
    bcrypt.compare(request.oldPassword, user.password).then( async (result) => {
        if (result) {
            user = {...user, 
            password: bcrypt.hashSync(request.newPassword, 10)
            }
            Utils.dbSave(Singleton.getInstance().userRepo, user)
            Utils.deleteAnotherTokens(Singleton.getInstance().tokenRepo, user, request.token as string);
            res.send({
                message: "ok",
                status: 200
            })
        } else {
            res.status(400).send({
                message: 'wrong password',
                status: 400
            })
        }
    })
})

settingsRouter.patch('/setbio', async (req, res, next) => {
    const request = {
        token: req.headers.token,
        bio: req.body.bio
    }

    let user = await Utils.findUserByToken(Singleton.getInstance().tokenRepo, request.token as string);

    user = await Utils.dbSave(Singleton.getInstance().userRepo, {
        ...user,
        bio: request.bio
    })

    res.send({
        message: "ok",
        status: 200,
        ...user,
        password: null
    })
})

settingsRouter.post('/setava', async (req, res, next) => {
    try {
        await new Promise((resolve, reject) =>{
            upload(req, res, function (err) {
                if (err instanceof multer.MulterError) {
                    console.log('multer')
                    reject(err)
                }
                resolve()
            })
        })
    } catch (error) {
      return  res.status(400).send(error)
    }

    const request = {
        filedata: req.file,
        token: req.headers.token,
    }

    if (!request.filedata) {
        res.status(400).send({
            message: 'uploading error. No file',
            status: 400
        })
        return
    }

    const user = await Utils.findUserByToken(Singleton.getInstance().tokenRepo, request.token as string);
    let avatar = await Utils.findUsersAvatarId(Singleton.getInstance().avatarsRepo, user);
    
    if(avatar) {
        fs.unlink(avatar.path, err => {
            if (err) {
                console.log('some problems with deleting file')
                res.status(400).send(err)
                return
            }
            console.log('file has been deleted')
        })
        avatar =  await Utils.dbSave(Singleton.getInstance().avatarsRepo, {
           ...avatar,
           path: request.filedata.path
       })
    } else {
        avatar = await Utils.dbSave(Singleton.getInstance().avatarsRepo, {
            user,
            path: request.filedata.path
        })
    }
    
    const response = {
        message: 'ok',
        status: 200,
        avatarId: avatar.id,
        createdDate: avatar.createdDate,
        updatedDate: avatar.updatedDate,
    }

    res.send({
        response
    })
})

export default settingsRouter;