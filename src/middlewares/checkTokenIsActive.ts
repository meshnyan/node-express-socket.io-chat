import * as Utils from '../helpers/utils'
import { Singleton } from '../helpers/singleton'

const checkTokenIsActive = async (req, res, next) => {
    if (req.headers.token) {
        const token = await Utils.isTokenActive(Singleton.getInstance().tokenRepo, req.headers.token as string)
        if (token) {
            next();
        }
        else {
            res.status(400).send({
                message: 'Request has unactive or invalid token. Put it in the request`s headers correct token, sign up or sign in',
                status: 400
            })
        }
    }
    else {
        res.status(400).send({
            message: 'Request has no token. Put it in the request`s headers, sign up or sign in',
            status: 400
        })
    }
}

export default checkTokenIsActive