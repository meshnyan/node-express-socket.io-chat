import express from 'express';
import socketIo, { Socket } from 'socket.io';
import http from 'http';
import 'reflect-metadata';
import bodyParser from 'body-parser';
import cors from 'cors'
import {CronJob} from 'cron'
import net from 'net'

import * as Utils from './helpers/utils';
import { Singleton } from './helpers/singleton'
import {dbConnection} from './db/connection';
import {TokenStatus} from './entities/token'
import authRouter from './middlewares/authRouter'
import chatRouter from './middlewares/chatRouter';
import settingsRouter from './middlewares/settingsRouter'
import { ISocketExtended } from './Interfaces/ExtSocket';
import validate from 'uuid-validate';
import { MessageStatus, Messages } from './entities/messages';

dbConnection.then(
    connection => {
///Переменные
        Singleton.getInstance(connection)
        const PORT = process.env.PORT || 3001;
        let userSocketMap: {
            [key: string]: Array<string>
        } = {};
        const app = express();
        const server = http.createServer(app);
        const io = socketIo(server, {
            pingInterval: 1000,
            pingTimeout: 10000
        })

///cron job
        const signInTimeout = new CronJob('0 0 */1 * * *', async () => {
            console.log('sign in timeout')
            const date = new Date()
            const tokens = await Utils.findAllActiveTokens(Singleton.getInstance().tokenRepo);
            tokens.forEach(value => {
                if((date.valueOf() - value.lastActivity.valueOf()) > 1000*60*60*4) {
                    Utils.dbSave(Singleton.getInstance().tokenRepo, {
                        id: value.id,
                        status: TokenStatus.DELETED
                    })
                }
            })
        })

        signInTimeout.start();

/// Логика  
        app.disable('x-powered-by');

        app.use(bodyParser.urlencoded({ extended: false}));
        app.use(bodyParser.json(), (err, req, res, next) => {
            if (err) {
                res.status(400).send({
                    message: 'Invalid JSON',
                    status: 400
                })
            } else {
                next();
            }
        });
        app.use(cors());
        
        app.use('/', authRouter);
        app.use('/chat', chatRouter);
        app.use('/settings', settingsRouter);

        app.use((err, req, res, next) => {
            console.error(err.stack);
            res.status(500).send('Asgard has been defeated')
        })

        io.on('connection', async (socket: ISocketExtended) => {
             if(validate(socket.handshake.query.token, 4)) {
                if(await Utils.isTokenActive(Singleton.getInstance().tokenRepo, socket.handshake.query.token)) {
                    const user = await Utils.findUserByToken(Singleton.getInstance().tokenRepo, socket.handshake.query.token);
                    userSocketMap[user.id] = userSocketMap[user.id] ? userSocketMap[user.id] : [];
                    userSocketMap[user.id].push(socket.id)
                    socket.client.user = user;
                } else {
                    socket.disconnect();
                }
            } else {
                socket.disconnect();
            }

            socket.on('message', async (data) => {
                if(validate(socket.handshake.query.token, 4)) {
                    if(Utils.isTokenActive(Singleton.getInstance().tokenRepo, socket.handshake.query.token)) {
                        
                        if(validate(data.to, 4)) {
                            const peer = await Utils.findUserById(Singleton.getInstance().userRepo, data.to);
                            if (peer) {
                                const message = await Utils.saveMessage(Singleton.getInstance().messagesRepo, socket.client.user.id, data.to, data.text);
                                if(!await Utils.isPeer(Singleton.getInstance().peersRepo, peer, socket.client.user.id)) {
                                    await Utils.dbSave(Singleton.getInstance().peersRepo, {
                                        user: peer,
                                        peerId: socket.client.user.id,
                                    })
                                }

                                const peersPeer = await Utils.findUsersPeerById(Singleton.getInstance().peersRepo, peer, socket.client.user.id);
                                Utils.dbSave(Singleton.getInstance().peersRepo, {
                                    ...peersPeer,
                                    isUnreadMessages: true,
                                    lastMessageFrom: message.from,
                                    lastMessageText: message.text
                                })

                                const usersPeer = await Utils.findUsersPeerById(Singleton.getInstance().peersRepo, socket.client.user, peer.id);
                                Utils.dbSave(Singleton.getInstance().peersRepo, {
                                    ...usersPeer,
                                    lastMessageFrom: message.from,
                                    lastMessageText: message.text
                                })

                                if (userSocketMap[data.to]) {
                                    if (userSocketMap[data.to].length) {
                                        userSocketMap[data.to].forEach(item => {
                                            io.sockets.connected[item].send(message)
                                        })
                                    }
                                }
                                
                                socket.emit('messageLoopback', message)
                            } else {
                                socket.emit('messageLoopback', {
                                    notification: 'Error. Peer with that ID is not exist'
                                })
                            }
                        } else {
                            socket.emit('messageLoopback', {
                                notification: 'Error. Peer ID is not valid'
                            })
                        }
                    } else {
                        userSocketMap[socket.client.user.id] = userSocketMap[socket.client.user.id].filter((item) => {
                            item !== socket.id
                        })
                        socket.disconnect();
                    }
                } else {
                    socket.disconnect();
                }
            })

            socket.on('read', async (data) => {
                const user = await Utils.findUserByToken(Singleton.getInstance().tokenRepo, socket.handshake.query.token);
                const peer = await Utils.findUsersPeerById(Singleton.getInstance().peersRepo, user, data.from);
                
                const readMessages = await Utils.switchDeliveredStatusToRead(Singleton.getInstance().messagesRepo, Singleton.getInstance().peersRepo, user, peer)
                
                if (readMessages) {
                    if (readMessages.length) {
                        if (userSocketMap[data.from]) {
                            if (userSocketMap[data.from].length) {
                                userSocketMap[data.from].forEach((item) => {
                                    io.sockets.connected[item].emit('readMessagesLoopback', {
                                        readMessages,
                                        from: user.id
                                    })
                                })
                                userSocketMap[user.id].forEach((item) => {
                                    io.sockets.connected[item].emit('readMessagesLoopback', {
                                        readMessages,
                                        from: data.from
                                    })
                                })
                            }
                        }
                    }
                }
            })

            socket.on('disconnect', () => {
                userSocketMap[socket.client.user.id] = (userSocketMap[socket.client.user.id])
                .filter(item => {
                    return item !== socket.id
                })
            })
        });

        server.listen(PORT, () => {
            console.log(`\nServer is running in http://localhost:${PORT}\n`)
        })
})