import { 
    Entity, Column, PrimaryGeneratedColumn, OneToMany, OneToOne, CreateDateColumn, UpdateDateColumn, JoinColumn
} from "typeorm";
import { Token } from './token';
import { Peers } from "./peers";
import { Avatars } from './avatars'

@Entity()
export class User {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({
        unique: true
    }) 
    username: string;

    @Column()
    password: string;

    @Column({
        nullable: true
    })
    bio: string;

    @CreateDateColumn({
        type: 'timestamptz'
    })
    createdDate: Date;

    @UpdateDateColumn({
        type: 'timestamptz'
    })
    updatedDate: Date;

    @OneToMany(type => Peers, p => p.user)
    peers: Peers;

    @OneToMany(type => Token, t => t.user)
    token: Token;

    @OneToOne(type => Avatars, a => a.user)
    avatar: Avatars
}