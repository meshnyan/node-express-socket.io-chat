import { 
    Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, Timestamp
} from "typeorm";

export enum MessageStatus {
    UNDELIVERED = 'undelivered',
    DELIVERED = 'delivered',
    READ = 'read',
    DELETED = 'deleted'
}

@Entity()
export class Messages {
    @PrimaryGeneratedColumn('uuid') 
    id: string;

    @Column()
    from: string;

    @Column()
    to: string;

    @Column({
        type: 'text',
        nullable: true
    })
    text: string;

    @Column({
        type: 'enum',
        enum: MessageStatus,
        default: MessageStatus.UNDELIVERED
    })
    status: MessageStatus;

    @CreateDateColumn({
        type: 'timestamptz'
    })
    createdDate: Date ;

    @UpdateDateColumn({
        type: 'timestamptz'
    })
    updatedDate: Date;
}