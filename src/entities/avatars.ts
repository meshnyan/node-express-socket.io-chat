import { 
    Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToOne, JoinColumn
} from "typeorm";
import { User } from './user'

@Entity()
export class Avatars {
    @PrimaryGeneratedColumn('uuid') 
    id: string;

    @Column({
        nullable: true
    })
    path: string;

    @CreateDateColumn({
        type: 'timestamptz'
    })
    createdDate: Date;

    @UpdateDateColumn({
        type: 'timestamptz'
    })
    updatedDate: Date;

    @OneToOne(() => User, user => user.token)
    @JoinColumn()
    user: User;
}