import { 
    Entity, Column, ManyToOne, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn
} from "typeorm";
import { User } from './user';

@Entity()
export class Peers {
    @PrimaryGeneratedColumn('uuid') 
    id: string;

    @Column()
    peerId: string;

    @Column({
        default: false
    })
    deleted: Boolean;

    @Column({
        default: false
    })
    isUnreadMessages: Boolean;

    @Column({
        nullable: true
    })
    lastMessageFrom: string;

    @Column({
        nullable: true
    })
    lastMessageText: string;

    @CreateDateColumn({
        type: 'timestamptz'
    })
    createdDate: Date;

    @UpdateDateColumn({
        type: 'timestamptz'
    })
    updatedDate: Date;

    @ManyToOne(() => User, t => t.peers)
    user: User;
}