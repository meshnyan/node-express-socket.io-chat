import {Socket, Client} from 'socket.io'
import { User } from '../entities/user';

export interface ISocketExtended extends Socket {
    client: IClientWithUser
}

interface IClientWithUser extends Client {
    user?: User
}