import { Connection } from 'typeorm'
import { Avatars } from '../entities/avatars'

export const getAvatarsRepository = (connection: Connection) => connection.getRepository(Avatars)