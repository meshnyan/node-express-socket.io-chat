import { Connection } from 'typeorm'
import { Peers } from '../entities/peers'

export const getPeersRepository = (connection: Connection) => connection.getRepository(Peers)