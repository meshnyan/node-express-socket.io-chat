import { Connection } from 'typeorm'
import { Token } from '../entities/token'

export const getTokenRepository = (connection: Connection) => connection.getRepository(Token)