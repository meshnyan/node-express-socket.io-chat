import { Connection } from 'typeorm'
import { Messages } from '../entities/messages'

export const getMessagesRepository = (connection: Connection) => connection.getRepository(Messages)