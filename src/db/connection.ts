import { createConnection } from "typeorm";
// @ts-ignore
import * as entities from '../entities'

export const dbConnection = createConnection({
    type: 'postgres',
    "name": "default",
    "host": "localhost",
    "port": 5433,
    "username": "test",
    "password": "test",
    "database": "test",
    "synchronize": true,
    "logging": true,
    entities: Object.values(entities)
})